﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PlayWithWords.State
{
    public class CustomDict
    {
        public CustomDict()
        {

        }

        public void Update(GameTime gameTime)
        {

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            GameManager GM = GameManager.Instance;
            GraphicsDevice GD = GM.GraphicsDevice;
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Backgrounds/bg4_spring"), new Rectangle(0, 0, GD.Viewport.Width, GD.Viewport.Height), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/PwW_logo"), new Vector2(250, 10), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("CustomDict/scroll-600x380"), new Vector2(100, 98), Color.White);
        }
    }
}
