﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayWithWords.Controls;
using PlayWithWords.Tools;
using System;

namespace PlayWithWords.State
{
    public class GameOver
    {
        Button buttonHome;
        Button buttonPlayAgain;
        bool isHighScore;


        public GameOver()
        {
            // khởi tạo button trở về MainMenu
            buttonHome = new Button(330, 300, GameManager.Instance.Content.Load<Texture2D>("Button/07"));
            buttonHome.LeftButtonClick += ButtonHome_LeftButtonClick;

            // khởi tạo button chơi ván mới
            buttonPlayAgain = new Button(430, 300, GameManager.Instance.Content.Load<Texture2D>("Button/18"));
            buttonPlayAgain.LeftButtonClick += ButtonPlayAgain_LeftButtonClick;

            if (isHighScore = GameManager.Instance.mainGame.session_coin > GameManager.Instance.highScore)
                GameManager.Instance.highScore = GameManager.Instance.mainGame.session_coin;
        }

        private void ButtonPlayAgain_LeftButtonClick(object sender, EventArgs e)
        {
            GameManager.Instance.coin += GameManager.Instance.mainGame.session_coin;
            GameManager.Instance.mainGame = new MainGame();
            GameManager.Instance.CurrentGameState = GameState.GameStarted;
        }

        private void ButtonHome_LeftButtonClick(object sender, EventArgs e)
        {
            GameManager.Instance.coin += GameManager.Instance.mainGame.session_coin;
            GameManager.Instance.CurrentGameState = GameState.MainMenu;
        }

        public void Update(GameTime gameTime)
        {
            buttonHome.Update(gameTime);
            buttonPlayAgain.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            GameManager GM = GameManager.Instance;
            GraphicsDevice GD = GM.GraphicsDevice;
            //Vẽ background
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Backgrounds/bg2_summer"), new Rectangle(0, 0, GD.Viewport.Width, GD.Viewport.Height), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/PwW_logo"), new Vector2(250, 10), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/highscore_scroll"), new Vector2(265, 140), Color.White);

            Helper.DrawString(spriteBatch, Helper.ComicSansMS25, GameManager.Instance.mainGame.session_coin.ToString(), new Rectangle(265, 230, 300, 40), Helper.Alignment.Center, Color.Black);
            if (isHighScore)
            {
                spriteBatch.DrawString(Helper.ComicSansMS20, "BEST SCORE", new Vector2(330, 180), Color.Black);
            }
            else
            {

                spriteBatch.DrawString(Helper.ComicSansMS20, "YOUR SCORE", new Vector2(330, 180), Color.Black);
            }

            buttonHome.Draw(spriteBatch);
            buttonPlayAgain.Draw(spriteBatch);
        }
    }
}
