﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayWithWords.Controls;
using PlayWithWords.Tools;
using PlayWithWords.Objects;
using System.IO;

namespace PlayWithWords.State
{
    public class Mode
    {
        Button buttonBack;
        Button buttonDict1;
        Button buttonDict2;
        Button buttonDict3;
        Button buttonDict4;
        Button buttonAdd;
        Button buttonLvl1;
        Button buttonLvl2;
        Button buttonLvl3;
            
        //public object Sprites { get; private set; }
        public Dictionary<string, Sprite> Sprites;


        public Mode()
        {
            Sprites = new Dictionary<string, Sprite>();
            // khởi tạo button Back
            buttonBack = new Button(20, 410, GameManager.Instance.Content.Load<Texture2D>("Button/09"));
            buttonBack.LeftButtonClick += new EventHandler(ButtonBack_LeftClick);
            Sprites.Add("buttonBack", buttonBack);


            //Khởi tạo nút check
            buttonDict1 = new Button(300, 185, GameManager.Instance.getDict1Img());
            buttonDict1.LeftButtonClick += ButtonDict1_LeftButtonClick;
            Sprites.Add("buttonDict1", buttonDict1);

            buttonDict2 = new Button(300, 215, GameManager.Instance.getDict2Img());
            buttonDict2.LeftButtonClick += ButtonDict2_LeftButtonClick;
            Sprites.Add("buttonDict2", buttonDict2);

            buttonDict3 = new Button(300, 245, GameManager.Instance.getDict3Img());
            buttonDict3.LeftButtonClick += ButtonDict3_LeftButtonClick;
            Sprites.Add("buttonDict3", buttonDict3);

            buttonDict4 = new Button(300, 275, GameManager.Instance.getDict4Img());
            //buttonDict4.LeftButtonClick += ButtonDict4_LeftButtonClick;
            Sprites.Add("buttonDict4", buttonDict4);

            buttonLvl1 = new Button(300, 350, GameManager.Instance.getLevel1Img());
            buttonLvl1.LeftButtonClick += ButtonLvl1_LeftButtonClick;
            Sprites.Add("buttonLvl1", buttonLvl1);

            buttonLvl2 = new Button(300, 380, GameManager.Instance.getLevel2Img());
            buttonLvl2.LeftButtonClick += ButtonLvl2_LeftButtonClick;
            Sprites.Add("buttonLvl2", buttonLvl2);

            buttonLvl3 = new Button(300, 410, GameManager.Instance.getLevel3Img());
            buttonLvl3.LeftButtonClick += ButtonLvl3_LeftButtonClick;
            Sprites.Add("buttonLvl3", buttonLvl3);

            buttonAdd = new Button(320, 350, GameManager.Instance.Content.Load<Texture2D>("Button/14a"));
            buttonAdd.LeftButtonClick += ButtonAdd_LeftButtonClick;

            
        }

        private void ButtonLvl3_LeftButtonClick(object sender, EventArgs e)
        {
            switch (GameManager.Instance.levelStatus)
            {
                case 3:
                case 5:                
                    GameManager.Instance.levelStatus += 4;
                    break;
                case 7:
                case 9:
                    GameManager.Instance.levelStatus -= 4;
                    break;
            }                
            buttonLvl1.Image = GameManager.Instance.getLevel1Img();
            buttonLvl2.Image = GameManager.Instance.getLevel2Img();
            buttonLvl3.Image = GameManager.Instance.getLevel3Img();
            LoadVocal();
        }


        private void ButtonLvl2_LeftButtonClick(object sender, EventArgs e)
        {
            switch (GameManager.Instance.levelStatus)
            {
                case 2:
                    GameManager.Instance.levelStatus += 3;
                    break;
                case 5:
                    GameManager.Instance.levelStatus -= 3;
                    break;
            }
            buttonLvl1.Image = GameManager.Instance.getLevel1Img();
            buttonLvl2.Image = GameManager.Instance.getLevel2Img();
            buttonLvl3.Image = GameManager.Instance.getLevel3Img();
            LoadVocal();
        }

        private void ButtonLvl1_LeftButtonClick(object sender, EventArgs e)
        {
            switch (GameManager.Instance.levelStatus)
            {
                case 3:
                case 7:
                    GameManager.Instance.levelStatus += 2;
                    break;
                case 5:
                case 9:
                    GameManager.Instance.levelStatus -= 2;
                    break;
            }
            buttonLvl1.Image = GameManager.Instance.getLevel1Img();
            buttonLvl2.Image = GameManager.Instance.getLevel2Img();
            buttonLvl3.Image = GameManager.Instance.getLevel3Img();
            LoadVocal();
        }

        private void ButtonDict1_LeftButtonClick(object sender, EventArgs e)
        {
            GameManager.Instance.dictStatus = 1;
            buttonDict1.Image = GameManager.Instance.getDict1Img();
            buttonDict2.Image = GameManager.Instance.getDict2Img();
            buttonDict3.Image = GameManager.Instance.getDict3Img();
            buttonDict4.Image = GameManager.Instance.getDict4Img();
            GameManager.Instance.Voca_List.Clear();
            LoadVocal();
        }

        private void ButtonDict2_LeftButtonClick(object sender, EventArgs e)
        {
            GameManager.Instance.dictStatus = 2;
            buttonDict1.Image = GameManager.Instance.getDict1Img();
            buttonDict2.Image = GameManager.Instance.getDict2Img();
            buttonDict3.Image = GameManager.Instance.getDict3Img();
            buttonDict4.Image = GameManager.Instance.getDict4Img();
            LoadVocal();
        }

        private void ButtonDict3_LeftButtonClick(object sender, EventArgs e)
        {
            GameManager.Instance.dictStatus = 3;
            buttonDict1.Image = GameManager.Instance.getDict1Img();
            buttonDict2.Image = GameManager.Instance.getDict2Img();
            buttonDict3.Image = GameManager.Instance.getDict3Img();
            buttonDict4.Image = GameManager.Instance.getDict4Img();
            LoadVocal();
        }

        private void ButtonDict4_LeftButtonClick(object sender, EventArgs e)
        {            
            if (!Directory.Exists(Path.Combine(Directory.GetCurrentDirectory(), "Vocabulary\\Custom")))
            {
                Directory.CreateDirectory("Vocabulary\\Custom");
            }
            string filePath = Path.Combine(Directory.GetCurrentDirectory(), "Vocabulary\\Custom") + "\\custom.pww";
            if (!File.Exists(filePath))
            {
                File.WriteAllText(filePath, "");
            }
            if (File.ReadAllText(filePath) == "")
            {
                GameManager.Instance.customDict = new CustomDict();
                GameManager.Instance.CurrentGameState = GameState.CustomDict;
            }
            else
            {
                GameManager.Instance.dictStatus = 4;
                buttonDict1.Image = GameManager.Instance.getDict1Img();
                buttonDict2.Image = GameManager.Instance.getDict2Img();
                buttonDict3.Image = GameManager.Instance.getDict3Img();
                buttonDict4.Image = GameManager.Instance.getDict4Img();
                GameManager.Instance.Voca_List.Clear();
                VocabularyText voca = new VocabularyText();
                voca.LoadFromFile(GameManager.Instance.getVocaPath() + "\\custom.pww");
                GameManager.Instance.Voca_List.Add(voca);
            }
                 
        }

        private void ButtonAdd_LeftButtonClick(object sender, EventArgs e)
        {
            GameManager.Instance.customDict = new CustomDict();
            GameManager.Instance.CurrentGameState = GameState.CustomDict;
        }

        private void ButtonBack_LeftClick(object sender, EventArgs e)
        {
            GameManager.Instance.CurrentGameState = GameState.MainMenu;
        }
        
        private void LoadVocal()
        {
            GameManager.Instance.Voca_List.Clear();
            string[] st = Directory.GetFiles(GameManager.Instance.getVocaPath());
            foreach (string file in Directory.GetFiles(GameManager.Instance.getVocaPath()))
            {
                VocabularyText voca = new VocabularyText();
                voca.LoadXFromFile(file, GameManager.Instance.getNumberWords());
                GameManager.Instance.Voca_List.Add(voca);
            }
        }

        public void Update(GameTime gameTime)
        {
            if (GameManager.Instance.dictStatus == 4)
            {
                buttonAdd.Update(gameTime);
            }
            foreach (KeyValuePair<string, Sprite> sprite in Sprites)
            {
                sprite.Value.Update(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            GameManager GM = GameManager.Instance;
            GraphicsDevice GD = GM.GraphicsDevice;
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Backgrounds/bg4_spring"), new Rectangle(0, 0, GD.Viewport.Width, GD.Viewport.Height), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/PwW_logo"), new Vector2(250, 10), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Setting/scroll"), new Vector2(250, 110), Color.White);

            spriteBatch.DrawString(Helper.Critter, "Wordpool", new Vector2(300, 130), Color.Green);
            spriteBatch.DrawString(Helper.Pacifico, "50 Words", new Vector2(340, 170), Color.Brown);
            spriteBatch.DrawString(Helper.Pacifico, "200 Words", new Vector2(340, 200), Color.Brown);
            spriteBatch.DrawString(Helper.Pacifico, "450 Words", new Vector2(340, 230), Color.Brown);
            spriteBatch.DrawString(Helper.Pacifico, "Custom", new Vector2(340, 260), Color.Brown);

            spriteBatch.DrawString(Helper.Critter, "Level", new Vector2(350, 300), Color.DarkGoldenrod);
            spriteBatch.DrawString(Helper.Pacifico, "Beginner", new Vector2(340, 340), Color.Firebrick);
            spriteBatch.DrawString(Helper.Pacifico, "Intermediate", new Vector2(340, 370), Color.Firebrick);
            spriteBatch.DrawString(Helper.Pacifico, "Expert", new Vector2(340, 400), Color.Firebrick);

            if (GM.dictStatus == 4)
            {
                buttonAdd.Draw(spriteBatch);
                spriteBatch.DrawString(Helper.Arial14, "Modify Your Dictionary", new Vector2(360, 360), Color.WhiteSmoke);
            }

            foreach (KeyValuePair<string, Sprite> sprite in Sprites)
            {
                sprite.Value.Draw(spriteBatch);
            }

            int[] limit = GM.getLevel();
            Color[] color = new Color[] { Color.Red, Color.Green, Color.Blue};
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Skill/skillDescripPanel"), new Vector2(580, 120), Color.White);
            if (GM.dictStatus == 1 || GM.dictStatus == 2 || GM.dictStatus == 3)
            {
                spriteBatch.DrawString(Helper.Arial14, Helper.WrapText(Helper.Arial14,
                    "Create a pool of " +(GM.dictStatus*GM.dictStatus*50).ToString() + " words with " 
                    + limit[0].ToString() + " ~ " + limit[1].ToString() + " characters"
                    , 150), new Vector2(590, 200), color[GM.dictStatus-1]);
            }
                
            if (GM.dictStatus == 4)
            {
                spriteBatch.DrawString(Helper.Arial14, Helper.WrapText(Helper.Arial14,
                    "Create and modify your own pool of your own words. No coin can be earned from this mode!"
                    , 150), new Vector2(590, 200), Color.Yellow);
            }

            switch (GM.levelStatus)
            {
                case 2:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "Beginner", new Vector2(610, 130), Color.Black);
                    break;
                case 3:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "Intermediate", new Vector2(580, 130), Color.Black);
                    break;
                case 5:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "Advanced", new Vector2(595, 130), Color.Black);
                    break;
                case 7:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "Expert", new Vector2(620, 130), Color.Black);
                    break;
                case 9:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "All Trades", new Vector2(590, 130), Color.Black);
                    break;
            }            
        }
    }
}
