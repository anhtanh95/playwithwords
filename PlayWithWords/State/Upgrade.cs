﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayWithWords.Controls;
using PlayWithWords.Tools;
using PlayWithWords.Objects;
using Microsoft.Xna.Framework.Input;

namespace PlayWithWords.State
{
    public class Upgrade
    {
        Button buttonBack;
        Button skillBomber;
        Button skillRevealer;
        Button skillSlower;

        bool showDescripBg;
        bool showBomberDescrip;
        bool showRevealerDescrip;
        bool showSlowerDescrip;

        int bomberCost;
        int revealerCost;
        int slowerCost;

        Dictionary<string, Sprite> Sprites;

        public Upgrade()
        {
            showDescripBg = false;
            showBomberDescrip = false;
            showRevealerDescrip = false;
            showSlowerDescrip = false;

            // khởi tạo từ điển lưu giữ các đối tượng
            Sprites = new Dictionary<string, Sprite>();

            // khởi tạo skill Bomber - phá hủy 2 hàng sai
            skillBomber = new Button(280, 200, GameManager.Instance.getBomberImg());
            if (GameManager.Instance.bomberStatus == 5)
                bomberCost = 0;
            else
                bomberCost = GameManager.Instance.bomberStatus * 15;
            skillBomber.LeftButtonClick += new EventHandler(SkillBomber_LeftClick);
            skillBomber.MouseEnter += SkillBomber_MouseEnter;
            skillBomber.MouseLeave += SkillBomber_MouseLeave;
            Sprites.Add("skillBomber", skillBomber);

            // khởi tạo skill Revealer - hiện gợi ý của từ hiện tại
            skillRevealer = new Button(280, 270, GameManager.Instance.getRevealerImg());
            if (GameManager.Instance.revealerStatus == 5)
                revealerCost = 0;
            else
                revealerCost = GameManager.Instance.revealerStatus * 15;
            skillRevealer.LeftButtonClick += new EventHandler(SkillRevealer_LeftClick);
            skillRevealer.MouseEnter += SkillRevealer_MouseEnter;
            skillRevealer.MouseLeave += SkillRevealer_MouseLeave;
            Sprites.Add("skillHelper", skillRevealer);

            // khởi tạo skill Slower - giảm tốc độ rơi đến hết từ hiện tại
            skillSlower = new Button(280, 340, GameManager.Instance.getSlowerImg());
            if (GameManager.Instance.slowerStatus == 5)
                slowerCost = 0;
            else
                slowerCost = GameManager.Instance.slowerStatus * 15;
            skillSlower.LeftButtonClick += new EventHandler(SkillSlower_LeftClick);
            skillSlower.MouseEnter += SkillSlower_MouseEnter;
            skillSlower.MouseLeave += SkillSlower_MouseLeave;
            Sprites.Add("skillSlower", skillSlower);

            // khởi tạo button Back
            buttonBack = new Button(20, 410, GameManager.Instance.Content.Load<Texture2D>("Button/09"));
            buttonBack.LeftButtonClick += new EventHandler(ButtonBack_LeftClick);
            Sprites.Add("buttonBack", buttonBack);
        }

        private void SkillSlower_MouseLeave(object sender, EventArgs e)
        {
            showDescripBg = false;
            showSlowerDescrip = false;
        }

        private void SkillSlower_MouseEnter(object sender, EventArgs e)
        {
            showDescripBg = true;
            showSlowerDescrip = true;
        }

        private void SkillRevealer_MouseLeave(object sender, EventArgs e)
        {
            showDescripBg = false;
            showRevealerDescrip = false;
        }

        private void SkillRevealer_MouseEnter(object sender, EventArgs e)
        {
            showDescripBg = true;
            showRevealerDescrip = true;
        }

        private void SkillBomber_MouseLeave(object sender, EventArgs e)
        {
            showDescripBg = false;
            showBomberDescrip = false;
        }

        private void SkillBomber_MouseEnter(object sender, EventArgs e)
        {
            showDescripBg = true;
            showBomberDescrip = true;
        }

        private void SkillBomber_LeftClick(object sender, EventArgs e)
        {
            if (GameManager.Instance.bomberStatus < 5 && bomberCost <= GameManager.Instance.coin)
            {
                GameManager.Instance.coin -= bomberCost;
                GameManager.Instance.bomberStatus++;
                bomberCost = GameManager.Instance.bomberStatus * 15;
                skillBomber.Image = GameManager.Instance.getBomberImg();
            }
            if (GameManager.Instance.bomberStatus == 5)
                bomberCost = 0;
        }

        private void SkillRevealer_LeftClick(object sender, EventArgs e)
        {
            if (GameManager.Instance.revealerStatus < 5 && revealerCost <= GameManager.Instance.coin)
            {
                GameManager.Instance.coin -= revealerCost;
                GameManager.Instance.revealerStatus++;
                revealerCost = GameManager.Instance.revealerStatus * 15;
                skillRevealer.Image = GameManager.Instance.getRevealerImg();
            }
            if (GameManager.Instance.revealerStatus == 5)
                revealerCost = 0;
        }

        private void SkillSlower_LeftClick(object sender, EventArgs e)
        {
            if (GameManager.Instance.slowerStatus < 5 && slowerCost <= GameManager.Instance.coin)
            {
                GameManager.Instance.coin -= slowerCost;
                GameManager.Instance.slowerStatus++;
                slowerCost = GameManager.Instance.slowerStatus * 15;
                skillSlower.Image = GameManager.Instance.getSlowerImg();
            }
            if (GameManager.Instance.slowerStatus == 5)
                slowerCost = 0;
        }
        

        private void ButtonBack_LeftClick(object sender, EventArgs e)
        {
            GameManager.Instance.CurrentGameState = GameState.MainMenu;
        }

        public void Update(GameTime gameTime)
        {
            foreach (KeyValuePair<string, Sprite> sprite in Sprites)
            {
                sprite.Value.Update(gameTime);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.B) && Keyboard.GetState().IsKeyDown(Keys.A) && Keyboard.GetState().IsKeyDown(Keys.O))
            {
                GameManager.Instance.coin += 1340;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            GameManager GM = GameManager.Instance;
            GraphicsDevice GD = GM.GraphicsDevice;
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Backgrounds/bg4_spring"), new Rectangle(0, 0, GD.Viewport.Width, GD.Viewport.Height), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/PwW_logo"), new Vector2(250, 10), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Setting/scroll"), new Vector2(250, 110), Color.White);
            spriteBatch.DrawString(Helper.Critter, "UPGRADE", new Vector2(310, 140), Color.DarkRed);

            spriteBatch.DrawString(Helper.Arial14, bomberCost.ToString(), new Vector2(400, 215), Color.Brown);
            spriteBatch.DrawString(Helper.Arial14, revealerCost.ToString(), new Vector2(400, 285), Color.Firebrick);
            spriteBatch.DrawString(Helper.Arial14, slowerCost.ToString(), new Vector2(400, 355), Color.Brown);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Coin/coin"), new Vector2(440, 205), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Coin/coin"), new Vector2(440, 275), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Coin/coin"), new Vector2(440, 345), Color.White);

            spriteBatch.Draw(GM.Content.Load<Texture2D>("Coin/coin2"), new Vector2(20, 130), Color.White);
            spriteBatch.DrawString(Helper.Arial14, GameManager.Instance.coin.ToString(), new Vector2(90, 150), Color.Firebrick);

            foreach (KeyValuePair<string, Sprite> sprite in Sprites)
            {
                sprite.Value.Draw(spriteBatch);
            }

            if (showDescripBg)
                spriteBatch.Draw(GM.Content.Load<Texture2D>("Skill/skillDescripPanel"), new Vector2(580, 120), Color.White);
            if (showBomberDescrip)
            {
                spriteBatch.DrawString(Helper.ComicSansMS20, "Bomber", new Vector2(620, 130), Color.Black);
                spriteBatch.DrawString(Helper.Arial14, Helper.WrapText(Helper.Arial14,
                    "Instantly destroy 2 incorrect rows", 150), new Vector2(590, 200), Color.Purple);
            }
            if (showRevealerDescrip)
            {
                spriteBatch.DrawString(Helper.ComicSansMS20, "Revealer", new Vector2(600, 130), Color.Black);
                spriteBatch.DrawString(Helper.Arial14, Helper.WrapText(Helper.Arial14,
                    "Reveal 1/3 current word", 150), new Vector2(590, 200), Color.IndianRed);
            }
            if (showSlowerDescrip)
            {
                spriteBatch.DrawString(Helper.ComicSansMS20, "Slower", new Vector2(620, 130), Color.Black);
                spriteBatch.DrawString(Helper.Arial14, Helper.WrapText(Helper.Arial14,
                    "Slow drop speed for duration of the current word", 150), new Vector2(590, 200), Color.Cyan);
            }
        }
    }
}
