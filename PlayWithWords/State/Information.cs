﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayWithWords.Controls;
using PlayWithWords.Objects;
using PlayWithWords.Tools;
using System;
using System.Collections.Generic;

namespace PlayWithWords.State
{
    public class Information
    {
        MenuSign menuSign;

        Button skillBomber;
        Button skillRevealer;
        Button skillSlower;
        Button infoButton;

        List<Button> buttons = new List<Button>();
        public Dictionary<string, Sprite> Sprites;
        public Information()
        {
            Sprites = new Dictionary<string, Sprite>();
            //playButton.LeftButtonClick += new EventHandler(PlayButton_LeftClick);

            // khởi tạo skill Bomber - phá hủy 1 hàng sai
            skillBomber = new Button(590, 315, GameManager.Instance.getBomberImg());
            Sprites.Add("skillBomber", skillBomber);

            // khởi tạo skill Revealer - hiện gợi ý trong xx giây
            skillRevealer = new Button(655, 315, GameManager.Instance.getRevealerImg());
            Sprites.Add("skillHelper", skillRevealer);

            // khởi tạo skill Slower - giảm tốc độ rơi trong xx giây
            skillSlower = new Button(720, 315, GameManager.Instance.getSlowerImg());
            Sprites.Add("skillSlower", skillSlower);

            Texture2D image = GameManager.Instance.Content.Load<Texture2D>("Menu/menu_sign");
            menuSign = new MenuSign(60, 100, image);

            infoButton = new Button(730, 410, GameManager.Instance.Content.Load<Texture2D>("Button/05"));
            infoButton.LeftButtonClick += InfoButton_LeftButtonClick;
        }

        private void InfoButton_LeftButtonClick(object sender, EventArgs e)
        {
            GameManager.Instance.CurrentGameState = GameState.MainMenu;
        }

        public void Update(GameTime gameTime)
        {
            infoButton.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            GameManager GM = GameManager.Instance;
            GraphicsDevice GD = GM.GraphicsDevice;
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Backgrounds/bg1_winter"), new Rectangle(0, 0, GD.Viewport.Width, GD.Viewport.Height), Color.White);
            //spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/PwW_logo"), new Vector2(250, 10), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/highscore_scroll"), new Vector2(265, 140), Color.White);

            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/wood_board"), new Vector2(580, 110), Color.White);
            spriteBatch.Draw(GM.getBomberImg(), new Vector2(590, 115), Color.White);
            spriteBatch.Draw(GM.getRevealerImg(), new Vector2(655, 115), Color.White);
            spriteBatch.Draw(GM.getSlowerImg(), new Vector2(720, 115), Color.White);

            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/wood_board"), new Vector2(580, 210), Color.White);
            spriteBatch.DrawString(Helper.ComicSansMS20, "Wordpool:", new Vector2(620, 210), Color.Black);
            if (GameManager.Instance.dictStatus == 1)
                spriteBatch.DrawString(Helper.ComicSansMS20, "50 Words", new Vector2(620, 240), Color.Black);
            else if (GameManager.Instance.dictStatus == 2)
                spriteBatch.DrawString(Helper.ComicSansMS20, "200 Words", new Vector2(610, 240), Color.Black);
            else
                spriteBatch.DrawString(Helper.ComicSansMS20, "450 Words", new Vector2(610, 240), Color.Black);

            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/wood_board"), new Vector2(580, 310), Color.White);
            spriteBatch.DrawString(Helper.ComicSansMS20, "Level:", new Vector2(650, 310), Color.Black);
            switch (GM.levelStatus)
            {
                case 2:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "Beginner", new Vector2(625, 340), Color.Black);
                    break;
                case 3:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "Intermediate", new Vector2(600, 340), Color.Black);
                    break;
                case 5:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "Advanced", new Vector2(615, 340), Color.Black);
                    break;
                case 7:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "Expert", new Vector2(640, 340), Color.Black);
                    break;
                case 9:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "All Trades", new Vector2(610, 340), Color.Black);
                    break;
            }

            menuSign.Draw(spriteBatch);
            spriteBatch.DrawString(Helper.ComicSansMS20, "BEST SCORE", new Vector2(330, 180), Color.Black);
            Helper.DrawString(spriteBatch, Helper.ComicSansMS25, GameManager.Instance.highScore.ToString(), new Rectangle(265, 230, 300, 40), Helper.Alignment.Center, Color.Black);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Coin/coin2"), new Vector2(470, 280), Color.White);
            spriteBatch.DrawString(Helper.ComicSansMS20, "Coins:", new Vector2(300, 290), Color.Firebrick);
            Helper.DrawString(spriteBatch, Helper.ComicSansMS20, GameManager.Instance.coin.ToString(), new Rectangle(265, 290, 300, 40), Helper.Alignment.Center, Color.Firebrick);

            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/infoGame"), new Rectangle(0, 0, GD.Viewport.Width, GD.Viewport.Height), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/info_scroll"), new Vector2(100, 50), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/PwW_logo"), new Vector2(230, 60), Color.White);
            spriteBatch.DrawString(Helper.Fato, "Beta", new Vector2(570, 110), Color.Crimson);
            infoButton.Draw(spriteBatch);

            spriteBatch.DrawString(Helper.Arial14, GM.verGame, new Vector2(340, 160), Color.DarkCyan);

            spriteBatch.DrawString(Helper.Altus25, "Nhóm tác giả: ", new Vector2(160, 180), Color.Black);
            spriteBatch.DrawString(Helper.Altus20, "Dương Gia Bảo", new Vector2(170, 220), Color.ForestGreen);
            spriteBatch.DrawString(Helper.Altus20, "Trịnh Phương Hoa", new Vector2(170, 250), Color.DeepPink);
            spriteBatch.DrawString(Helper.Altus20, "Lê Thanh Huyền", new Vector2(170, 280), Color.DarkMagenta);
            spriteBatch.DrawString(Helper.Altus20, "Lê Minh Nghị", new Vector2(170, 310), Color.OrangeRed);
            spriteBatch.DrawString(Helper.Altus20, "Đặng Trần Tiến", new Vector2(170, 340), Color.Blue);
        
            spriteBatch.DrawString(Helper.Altus25, "Hướng dẫn: ", new Vector2(400, 180), Color.Black);
            spriteBatch.DrawString(Helper.Altus20, Helper.WrapText(Helper.Altus20,
                "Người chơi sử dụng các phím điều hướng để điều khiển các hộp đang rơi sao cho chúng tạo thành thứ tự đúng!",
                200), new Vector2(410, 220), Color.DarkRed);
        }
    }
}
