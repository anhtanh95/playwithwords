﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PlayWithWords.Controls;
using PlayWithWords.Events;
using PlayWithWords.Objects;
using PlayWithWords.Tools;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlayWithWords.State
{
    public class MainGame
    {
        Panel gamePanel;

        Button buttonBack;
        Button buttonSetting;
        Button skillBomber;
        Button skillRevealer;
        Button skillSlower;
        public bool isSlow;
        bool isReveal;
        int bombedIndex = -1;
        public int CurFloor { get; set; }
        Box currentBox;
        Box CurrentBox
        {
            get
            {
                return currentBox;
            }
            set
            {
                if (currentBox != null) currentBox.Focus = false;
                currentBox = value;
                if (currentBox != null) currentBox.Focus = true;
            }
        }
        public Dictionary<string, Sprite> Sprites;
        List<Floor> Floors;
        public int session_coin;

        int Counter;
        double waitBomber;
        string showReveal;
        string holdWord;
        int[] holdWordPos;
        string holdMeaing;

        public MainGame()
        {
            //For test purpose
            //Danh sách đối tướng: chứa tất cả các đối tượng Sprite có trên màn hình
            Sprites = new Dictionary<string, Sprite>();
            Floors = new List<Floor>(Helper.MaxFloor);
            //Con trỏ tầng: cho biết tầng đang xếp hộp hiện tại là tầng số bao nhiêu, mặc định khi khởi tạo là tầng 1
            CurFloor = 0;
            //Khởi tạo bộ đếm số hộp: cứ mỗi 1 hộp được tạo ra sẽ tăng biến đếm lên 1 đơn bị
            Counter = 0;
            waitBomber = 5000;
            isSlow = false;
            isReveal = false;

            //Khởi tạo panel chính của màn chơi
            Texture2D panelImage = GameManager.Instance.Content.Load<Texture2D>("MainGame/maingame");
            gamePanel = new Panel(270, 10, panelImage);
            Sprites.Add("gamePanel", gamePanel);

            //Khởi tạo nút Back quay trở lại Menu chính
            Texture2D buttonImage_Back = GameManager.Instance.Content.Load<Texture2D>("Button/09");
            buttonBack = new Button(20, 410, buttonImage_Back);
            buttonBack.LeftButtonClick += new EventHandler(BackButton_LeftClick);
            Sprites.Add("buttonBack", buttonBack);

            //Khởi tạo nút Setting 
            buttonSetting = new Button(90, 412, GameManager.Instance.Content.Load<Texture2D>("Button/06"));
            buttonSetting.LeftButtonClick += new EventHandler(SettingButton_LeftClick);
            Sprites.Add("buttonSetting", buttonSetting);


            // khởi tạo skill Bomber - phá hủy 1 hàng sai
            skillBomber = new Button(20, 340, GameManager.Instance.getBomberImg());
            skillBomber.LeftButtonClick += new EventHandler(SkillBomber_LeftClick);
            Sprites.Add("skillBomber", skillBomber);

            // khởi tạo skill Revealer - hiện gợi ý trong xx giây
            skillRevealer = new Button(90, 340, GameManager.Instance.getRevealerImg());
            skillRevealer.LeftButtonClick += new EventHandler(SkillRevealer_LeftClick);
            Sprites.Add("skillHelper", skillRevealer);

            // khởi tạo skill Slower - giảm tốc độ rơi trong xx giây
            skillSlower = new Button(160, 340, GameManager.Instance.getSlowerImg());
            skillSlower.LeftButtonClick += new EventHandler(SkillSlower_LeftClick);
            Sprites.Add("skillSlower", skillSlower);

            session_coin = 0;

            _testColor = new Texture2D(GameManager.Instance.GraphicsDevice, 1, 1);
            _testColor.SetData(new[] { Color.LightGray });
            //_textBox = new TextBox(
                //new Rectangle(15, 150, 240, 180),
                //200,
                //"aksjdhaksjdhaksjdhaksjdhaksjd",
                //GameManager.Instance.GraphicsDevice,
                //Helper.Arial14,
                //_testColor,
                //new Rectangle(0, 0, 1, 1));
            //System.Windows.Forms.TextBox txtBox = new System.Windows.Forms.TextBox();
            //txtBox.Location = new System.Drawing.Point(10, 10);
            //System.Windows.Forms.Control.FromHandle(GameManager.Instance.Window.Handle).Controls.Add(txtBox);

            //Bắt đầu sinh từ ngẫu nhiên
            GenerateNewWord();
        }

        ~MainGame()
        {

        }

        Texture2D _testColor;
        //TextBox _textBox;

        private void GenerateNewWord()
        {
            Random rnd = new Random();
            VocabularyText vocaText = GameManager.Instance.Voca_List[rnd.Next(0,GameManager.Instance.Voca_List.Count)];
            //VocabularyText vocaText = GameManager.Instance.Voca_List[0];
            int randomOrder = rnd.Next(vocaText.WordsList.Count);
            KeyValuePair<string, string> randomWord = vocaText.WordsList[randomOrder];
            Word newWord = new Word(randomWord.Key, randomWord.Value);
            holdWord = randomWord.Key;
            holdMeaing = randomWord.Value;
            Floors.Add(new Floor(newWord));
            GenerateNewBox();
            holdWordPos = new int[holdWord.Length];
            showReveal = "";
            for (int index = 0; index < randomWord.Key.Length; index++)
            {
                showReveal += "-";
                holdWordPos[index] = index;
            }
            for (int i = 1; i <= holdWord.Length / 3; i++)
            {
                int index = rnd.Next(0, holdWordPos.Length);
                showReveal = ChangeChar(holdWordPos[index], holdWord[holdWordPos[index]]);
                holdWordPos = RemoveAt(holdWordPos, index);
            }
        }



        private Box GenerateNewBox()
        {
            Box box = new Box();
            //Ta cho hộp bắt đầu rơi ở vị trí cột(cell) đang trống, tình từ trái qua phải, ta đặt vị trí này vào 
            //biến freeCell. Vì ta xét thứ tự cell tính theo trong hộp khung, mà hộp khung lại bắt đầu từ StartCell,
            //vậy thứ tự chính xác trong gamePanel sẽ là: n = StartCell + freeCell. Vậy vị trí X để vẽ hộp trong panel
            //sẽ bằng n * độ rộng của ảnh chiếc hộp.
            int freeCell = -1;
            for (int i = 0; i < Floors[CurFloor].Cells.Length; i++)
            {
                if (Floors[CurFloor].Cells[i] == null)
                {
                    freeCell = i;
                    break;
                }
            }
            int boxPosX = box.Image.Width * (Floors[CurFloor].StartCell + freeCell);
            //Vì ta cho hộp bắt đầu rời từ ngoài panel chính của trò chơi, vị trí y của panel chính ta coi là = 0, 
            //vì vậy vị trí y của hộp sẽ luôn bằng âm chiều cao của hộp (0 - box height) 
            int boxPosY = -box.Image.Height;
            box.Location = new Vector2(boxPosX, boxPosY);
            box.Cell = freeCell;
            box.Floor = CurFloor;
            box.ID = "box" + Counter;
            //Tạo các sự kiện cho hộp
            box.KeyHold += new CustomKeyEventHandler(Box_KeyHold);
            box.KeyUp += new CustomKeyEventHandler(Box_KeyUp);
            //Add hộp vừa tạo vào gamePanel
            gamePanel.AddSprite(box.ID, box);
            //Đặt kí tự vào hộp
            box.SetCharacter(Floors[CurFloor].Word.RandomOrder[Floors[CurFloor].Word.GetCurrentChar()]);
            //Tăng biến đếm Counter lên
            Counter++;

            //Đặt con trỏ hộp hiện tại (CurrentBox) vào hộp vừa mới tạo, sau đó cho hộp đó thực hiện hành động rơi (Drop)
            CurrentBox = box;
            CurrentBox.Drop();
            return box;
        }

        private void Box_KeyHold(object sender, CustomKeyEventArgs e)
        {
            //Speed-up Box's dropping speed when hold Down key
            int maxDown = gamePanel.Height - ((CurFloor + 1) * CurrentBox.Image.Height);
            if (CurrentBox.Location.Y < maxDown && e.Key == Keys.Down)
            {
                //Drop faster x5 time
                CurrentBox.Location.Y += 5;
                //Location Y cannot pass over maxDown value
                CurrentBox.Location.Y = Math.Min(CurrentBox.Location.Y, maxDown);
                //GameManager.Instance.soundEffects[4].CreateInstance().Play();
            }

        }

        private void Box_KeyUp(object sender, CustomKeyEventArgs e)
        {
            int nextLeftCell = -1, nextRightCell = -1;
            for (int i = CurrentBox.Cell - 1; i >= 0; i--)
            {
                if (Floors[CurFloor].Cells[i] == null)
                {
                    nextLeftCell = i;
                    break;
                }
            }
            for (int i = CurrentBox.Cell + 1; i < Floors[CurFloor].Cells.Length; i++)
            {
                if (Floors[CurFloor].Cells[i] == null)
                {
                    nextRightCell = i;
                    break;
                }
            }
            int minLeft = CurrentBox.Image.Width * Floors[CurFloor].StartCell;
            int maxRight = CurrentBox.Image.Width * (Floors[CurFloor].StartCell + Floors[CurFloor].Word.Count - 1);
            if (nextLeftCell != -1 && CurrentBox.Location.X > minLeft && e.Key == Keys.Left)
            {
                CurrentBox.Location.X = GetXByCellIdx(CurrentBox.Image, nextLeftCell);
                CurrentBox.Cell = nextLeftCell;
            }
            else
            if (nextRightCell != -1 && CurrentBox.Location.X < maxRight && e.Key == Keys.Right)
            {
                CurrentBox.Location.X = GetXByCellIdx(CurrentBox.Image, nextRightCell);
                CurrentBox.Cell = nextRightCell;
            }
            GameManager.Instance.soundEffects[3].CreateInstance().Play();
        }

        private void BackButton_LeftClick(object sender, EventArgs e)
        {

            GameManager.Instance.coin += session_coin;
            GameManager.Instance.CurrentGameState = GameState.MainMenu;
        }

        private void SettingButton_LeftClick(object sender, EventArgs e)
        {
            GameManager.Instance.LastGameState = GameState.GameStarted;
            GameManager.Instance.CurrentGameState = GameState.Setting;
        }

        private void SkillBomber_LeftClick(object sender, EventArgs e)
        {
            if (GameManager.Instance.bomberStatus > 2 && Floors.Count > 2)
            {
                GameManager.Instance.bomberStatus--;
                skillBomber.Image = GameManager.Instance.getBomberImg();
                Random rnd = new Random();
                bombedIndex = rnd.Next(Floors.Count - 1);//không lấy floor trên cùng
                Floors[bombedIndex].Destroy();
                waitBomber = 0;                
            }
        }

        private void SkillRevealer_LeftClick(object sender, EventArgs e)
        {
            if (GameManager.Instance.revealerStatus > 2 && !isReveal)
            {
                GameManager.Instance.revealerStatus--;
                skillRevealer.Image = GameManager.Instance.getRevealerImg();
                showReveal = holdWord;
                isReveal = true;
            }
        }

        private string ChangeChar(int v1, char v2)
        {
            char[] clone = showReveal.ToCharArray();
            clone[v1] = v2;
            string des = new string(clone);
            return des;
        }

        private int[] RemoveAt(int[] source, int index)
        {
            int[] dest = new int[source.Length - 1];
            if (index > 0)
                Array.Copy(source, 0, dest, 0, index);

            if (index < source.Length - 1)
                Array.Copy(source, index + 1, dest, index, source.Length - index - 1);

            return dest;
        }

        private void SkillSlower_LeftClick(object sender, EventArgs e)
        {
            if (GameManager.Instance.slowerStatus > 2 && !isSlow)
            {
                GameManager.Instance.slowerStatus--;
                skillSlower.Image = GameManager.Instance.getSlowerImg();
                isSlow = true;
            }
        }

        public void Update(GameTime gameTime)
        {
            // biến waitBomber sẽ bắt đầu lấy tgian theo game khi ấn skillBomber
            // đạt 2000milisecond thì thực hiện hành động xóa hàng lần nữa
            // khi đạt trên 2000millisecond thì ko cộng dồn tgian game vào biến nữa
            if (waitBomber < 2000)
            {
                waitBomber += gameTime.ElapsedGameTime.Milliseconds;
                if (waitBomber == 2000)
                {
                    Random rnd = new Random();
                    bombedIndex = rnd.Next(Floors.Count - 1);//không lấy floor trên cùng
                    Floors[bombedIndex].Destroy();
                }
            }

            foreach (KeyValuePair<string, Sprite> sprite in Sprites)
            {
                sprite.Value.Update(gameTime);
            }
            for (int i = 0; i < Floors.Count; i++)
            {
                Floors[i].Update();
            }
            if (GameManager.Instance.CurrentGameState != GameState.GamePaused
                && GameManager.Instance.CurrentGameState != GameState.GameOver)
            {
                //Kiểm tra trạng thái của hộp, nếu trạng thái là đã drop xuống đáy và số tầng hiện tại không vượt quá
                //số tầng tối đa
                if (CurrentBox != null && CurrentBox.BoxState == BoxState.Dropped && CurFloor < Helper.MaxFloor)
                {
                    //Nếu chưa rơi kí tự cuối cùng xuống thì tiếp tục sinh ra hộp chứa kí tự tiếp theo để drop xuống
                    if (Floors[CurFloor].Word.GetCurrentChar() != -1)
                    {
                        //Khi hộp hiện tại đã rơi xuống dưới sàn, thì đưa hộp hiện tại vào khung với vị trí tương ứng.
                        Floors[CurFloor].Cells[CurrentBox.Cell] = CurrentBox;
                        //Nếu kí tự tiếp theo == -1 nghĩa là đã hết kí tự -> không tạo hộp mới nữa
                        if (Floors[CurFloor].Word.NextChar() != -1)
                        {
                            GenerateNewBox();
                        }
                    }
                    else
                    {
                        //Còn không thì kiểm tra từ đã xếp đúng hay sai
                        if (Floors[CurFloor].CheckWordOrder())
                        {
                            //Phá hủy các hộp ở tầng hiện tại khi đã xếp đúng
                            session_coin += Floors[CurFloor].Word.EMeaning.Length;
                            Floors[CurFloor].Destroy();
                        }
                        else
                        {
                            //Faded các thùng ở tầng hiện tại nếu xếp sai
                            Floors[CurFloor].Disable();
                        }
                        CurrentBox = null;
                        isSlow = false;
                        isReveal = false;
                    }
                }
                if (Floors[CurFloor].CurrentState == FloorState.Destroyed
                    || Floors[CurFloor].CurrentState == FloorState.Disabled)
                {
                    if (Floors[CurFloor].CurrentState == FloorState.Destroyed)
                    {
                        foreach (Box box in Floors[CurFloor].Cells)
                        {
                            gamePanel.Sprites.Remove(box.ID);
                        }
                        Floors.Remove(Floors[CurFloor]);
                        GenerateNewWord();
                    }
                    else
                    {
                        //Correct the last word
                        for (int i = 0; i < Floors[CurFloor].Word.Count; i++)
                        {
                            Box box = Floors[CurFloor].Cells[i];
                            box.SetCharacter(Floors[CurFloor].Word.OriginOrder[i]);
                        }
                        CurFloor++;
                        if (CurFloor < Helper.MaxFloor)
                        {
                            GenerateNewWord();
                        }
                        else
                        {
                            GameManager.Instance.gameOver = new GameOver();
                            GameManager.Instance.CurrentGameState = GameState.GameOver;
                        }
                    }
                }
                if (bombedIndex != -1 && Floors[bombedIndex].CurrentState == FloorState.Destroyed)
                {
                    foreach (Box box in Floors[bombedIndex].Cells)
                    {
                        gamePanel.Sprites.Remove(box.ID);
                    }
                    Floors.Remove(Floors[bombedIndex]);

                    for (int i = bombedIndex; i < CurFloor; i++)
                    {
                        foreach (Box box in Floors[i].Cells)
                        {
                            if (box != null)
                            {
                                box.Floor = i;
                                Floors[i].Drop();
                            }
                        }
                    }
                    CurrentBox.Floor--;
                    CurFloor--;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            GameManager GM = GameManager.Instance;
            GraphicsDevice GD = GM.GraphicsDevice;
            //Vẽ background
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Backgrounds/bg2_summer"), new Rectangle(0, 0, GD.Viewport.Width, GD.Viewport.Height), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("MainGame/maingame_250x400"), new Vector2(10, 10), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("MainGame/trackbar2"), new Rectangle(260, 70, 540, 10), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Coin/coin"), new Vector2(20, 30), Color.White);
            spriteBatch.DrawString(Helper.Arial14, session_coin.ToString(), new Vector2(120, 40), Color.White);
            //Vẽ các hộp khung
            if (Floors.Count > 0 && CurFloor < Helper.MaxFloor)
            {
                Texture2D boxPlatform = GameManager.Instance.Content.Load<Texture2D>("Box/09");
                for (int i = 0; i < Floors[CurFloor].Word.Count; i++)
                {
                    if (Floors[CurFloor].Cells[i] == null)
                    {
                        spriteBatch.Draw(boxPlatform,
                            new Rectangle(
                                (int)gamePanel.Location.X + GetXByCellIdx(boxPlatform, i),
                                (int)gamePanel.Location.Y + GetYByFloorIdx(boxPlatform, CurFloor),
                                boxPlatform.Width,
                                boxPlatform.Height),
                            Color.White);
                    }
                }
            }

            spriteBatch.Draw(GM.Content.Load<Texture2D>("MainGame/textbox_bg"), new Vector2(20, 110), Color.White);
            spriteBatch.DrawString(Helper.Arial14, Helper.WrapText(Helper.Arial14, holdMeaing, 200), new Vector2(25, 130), Color.White);
            spriteBatch.DrawString(Helper.Fato, showReveal.ToUpper(), new Vector2(20, 300), Color.Black);

            foreach (KeyValuePair<string, Sprite> sprite in Sprites)
            {
                sprite.Value.Draw(spriteBatch);
            }

            //spriteBatch.Draw(_testColor, _textBox.Area, Color.Tan);
            //_textBox.Draw(spriteBatch);
        }

        public int GetXByCellIdx(Texture2D image, int cellIdx)
        {
            return image.Width * (Floors[CurFloor].StartCell + cellIdx);
        }

        public int GetYByFloorIdx(Texture2D image, int index)
        {
            return gamePanel.Height - ((index + 1) * image.Height);
        }
    }
}
