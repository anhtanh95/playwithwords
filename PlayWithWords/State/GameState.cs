﻿namespace PlayWithWords.State
{
    public enum GameState
    {
        MainMenu,
        GameStarted,
        GamePaused,
        GameOver,
        Setting,
        Upgrade,
        Mode,
        CustomDict,
        Info
    }
}
