﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayWithWords.Controls;
using PlayWithWords.Tools;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace PlayWithWords.State
{
    public class Setting
    {
        Button buttonBack;
        Button buttonSound;
        Button buttonMusic;


        public Setting()
        {

            //Khởi tạo nút Sound
            buttonSound = new Button(280, 270, GameManager.Instance.getSoundImg());
            buttonSound.LeftButtonClick += new EventHandler(SoundButton_LeftClick);

            //Khởi tạo nút Muisc
            buttonMusic = new Button(280, 200, GameManager.Instance.getMusicImg());
            buttonMusic.LeftButtonClick += new EventHandler(MusicButton_LeftClick);

            //Khởi tạo nút Back quay trở lại Menu chính
            Texture2D buttonImage_Back = GameManager.Instance.Content.Load<Texture2D>("Button/09");
            buttonBack = new Button(20, 410, buttonImage_Back);
            buttonBack.LeftButtonClick += new EventHandler(BackButton_LeftClick);
        }

        private void SoundButton_LeftClick(object sender, EventArgs e)
        {
            if (GameManager.Instance.activeSound)
            {
                GameManager.Instance.activeSound = false;
                SoundEffect.MasterVolume = 0.0f;
            } 
            else
            {
                GameManager.Instance.activeSound = true;
                SoundEffect.MasterVolume = 1.0f;
            }
            buttonSound.Image = GameManager.Instance.getSoundImg();
        }

        private void MusicButton_LeftClick(object sender, EventArgs e)
        {
            if (GameManager.Instance.activeMusic)
            {
                GameManager.Instance.activeMusic = false;
                MediaPlayer.Volume = 0.0f;


            }
                
            else
            {
                GameManager.Instance.activeMusic = true;
                MediaPlayer.Volume = 1.0f;
            }
            buttonMusic.Image = GameManager.Instance.getMusicImg();
        }

        private void BackButton_LeftClick(object sender, EventArgs e)
        {
            GameManager.Instance.CurrentGameState = GameManager.Instance.LastGameState;
        }

        public void Update(GameTime gameTime)
        {
            buttonBack.Update(gameTime);
            buttonSound.Update(gameTime);
            buttonMusic.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            GameManager GM = GameManager.Instance;
            GraphicsDevice GD = GM.GraphicsDevice;
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Backgrounds/bg3_autumn"), new Rectangle(0, 0, GD.Viewport.Width, GD.Viewport.Height), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/PwW_logo"), new Vector2(250, 10), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Setting/scroll"), new Vector2(250, 110), Color.White);
            spriteBatch.DrawString(Helper.Critter, "SETTINGS", new Vector2(310, 140), Color.DarkSlateBlue);
            spriteBatch.DrawString(Helper.Albino, "Music", new Vector2(355, 215), Color.Firebrick);
            spriteBatch.DrawString(Helper.Albino, "Sound", new Vector2(355, 285), Color.Firebrick);

            buttonBack.Draw(spriteBatch);
            buttonSound.Draw(spriteBatch);
            buttonMusic.Draw(spriteBatch);
        }
    }
}
