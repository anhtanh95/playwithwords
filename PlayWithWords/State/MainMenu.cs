﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayWithWords.Controls;
using PlayWithWords.Tools;
using System.Collections.Generic;
using PlayWithWords.Objects;
using System.IO;

namespace PlayWithWords
{
    public class MainMenu
    {
        MenuSign menuSign;

        public MainMenu()
        {                                    
            Texture2D image = GameManager.Instance.Content.Load<Texture2D>("Menu/menu_sign");
            menuSign = new MenuSign(60, 100, image);
        }



        ~MainMenu()
        {
            GameManager.Instance.gameInfo[0] = MD5.Encrypt(GameManager.Instance.verConfig, true);
            GameManager.Instance.gameInfo[1] = MD5.Encrypt(Convert.ToString(GameManager.Instance.bomberStatus), true);
            GameManager.Instance.gameInfo[2] = MD5.Encrypt(Convert.ToString(GameManager.Instance.revealerStatus), true);
            GameManager.Instance.gameInfo[3] = MD5.Encrypt(Convert.ToString(GameManager.Instance.slowerStatus), true);
            GameManager.Instance.gameInfo[4] = MD5.Encrypt(Convert.ToString(GameManager.Instance.dictStatus), true);
            GameManager.Instance.gameInfo[5] = MD5.Encrypt(Convert.ToString(GameManager.Instance.levelStatus), true);
            GameManager.Instance.gameInfo[6] = MD5.Encrypt(Convert.ToString(GameManager.Instance.coin), true);
            GameManager.Instance.gameInfo[7] = MD5.Encrypt(Convert.ToString(GameManager.Instance.highScore), true);
            GameManager.Instance.gameInfo[8] = MD5.Encrypt(Convert.ToString(GameManager.Instance.activeSound), true);
            GameManager.Instance.gameInfo[9] = MD5.Encrypt(Convert.ToString(GameManager.Instance.activeMusic), true);
            File.WriteAllLines(@"config.pww", GameManager.Instance.gameInfo);
        }

        public void Update(GameTime gameTime)
        {
            menuSign.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            GameManager GM = GameManager.Instance;
            GraphicsDevice GD = GM.GraphicsDevice;
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Backgrounds/bg1_winter"), new Rectangle(0, 0, GD.Viewport.Width, GD.Viewport.Height), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/PwW_logo"), new Vector2(250, 10), Color.White);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/highscore_scroll"), new Vector2(265, 140), Color.White);
            
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/wood_board"), new Vector2(580, 110), Color.White);
            spriteBatch.Draw(GM.getBomberImg(), new Vector2(590, 115), Color.White);
            spriteBatch.Draw(GM.getRevealerImg(), new Vector2(655, 115), Color.White);
            spriteBatch.Draw(GM.getSlowerImg(), new Vector2(720, 115), Color.White);

            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/wood_board"), new Vector2(580, 210), Color.White);
            spriteBatch.DrawString(Helper.ComicSansMS20, "Wordpool:", new Vector2(620, 210), Color.Black);
            if (GameManager.Instance.dictStatus == 1)
                spriteBatch.DrawString(Helper.ComicSansMS20, "50 Words", new Vector2(620, 240), Color.Black);
            else if (GameManager.Instance.dictStatus == 2)
                spriteBatch.DrawString(Helper.ComicSansMS20, "200 Words", new Vector2(610, 240), Color.Black);
            else
                spriteBatch.DrawString(Helper.ComicSansMS20, "450 Words", new Vector2(610, 240), Color.Black);

            spriteBatch.Draw(GM.Content.Load<Texture2D>("Menu/wood_board"), new Vector2(580, 310), Color.White);
            spriteBatch.DrawString(Helper.ComicSansMS20, "Level:", new Vector2(650, 310), Color.Black);
            switch (GM.levelStatus)
            {
                case 2:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "Beginner", new Vector2(625, 340), Color.Black);
                    break;
                case 3:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "Intermediate", new Vector2(600, 340), Color.Black);
                    break;
                case 5:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "Advanced", new Vector2(615, 340), Color.Black);
                    break;
                case 7:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "Expert", new Vector2(640, 340), Color.Black);
                    break;
                case 9:
                    spriteBatch.DrawString(Helper.ComicSansMS20, "All Trades", new Vector2(610, 340), Color.Black);
                    break;
            }

            menuSign.Draw(spriteBatch);
            spriteBatch.DrawString(Helper.ComicSansMS20, "BEST SCORE", new Vector2(330, 180), Color.Black);
            Helper.DrawString(spriteBatch, Helper.ComicSansMS25, GameManager.Instance.highScore.ToString(), new Rectangle(265, 230, 300, 40), Helper.Alignment.Center, Color.Black);
            spriteBatch.Draw(GM.Content.Load<Texture2D>("Coin/coin2"), new Vector2(470, 280), Color.White);
            spriteBatch.DrawString(Helper.ComicSansMS20, "Coins:", new Vector2(300, 290), Color.Firebrick);
            Helper.DrawString(spriteBatch, Helper.ComicSansMS20, GameManager.Instance.coin.ToString(), new Rectangle(265, 290, 300, 40), Helper.Alignment.Center, Color.Firebrick);
        }
    }
}