﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayWithWords.State;
using PlayWithWords.Tools;
using Drawing = System.Drawing;
using Forms = System.Windows.Forms;
using System.IO;
using System;
using System.Collections.Generic;
using PlayWithWords.Objects;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace PlayWithWords
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameManager : Game
    {
        public static GameManager Instance;

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public MainMenu mainMenu;
        public MainGame mainGame;
        public Setting settingGame;
        public Upgrade upgradeGame;
        public CustomDict customDict;
        public Mode gameMode;
        public GameOver gameOver;
        public Information infoGame;

        public List<VocabularyText> Voca_List; // list các từ sẽ xuất hiện trong MainGame
        public List<SoundEffect> soundEffects; // list các SoundEffect có thể sử dụng để tạo hiệu ứng

        // khởi tạo tình trạng skill , 1: locked, 2: available, 3: charged x1, 4: charged x2, 5: charged x3
        public int bomberStatus; // trạng thái skill bomber
        public int revealerStatus; // trạng thái skill helper
        public int slowerStatus; // trạng thái skill slower

        public int dictStatus; // khởi tạo biến giữ tình trạng từ điển, 1: 50 từ, 2: 200 từ, 3: 450 từ, 4: modify 
        public string vocaPath; // giữ đường dẫn tới folder Vocabulary
        public int levelStatus; // giữ tình trạng số lượng chữ cái mỗi từ, 2: beginner, 3: intermediate, 5: advanced, 7: expert, 9: (jack of) all trades

        // khởi tạo tình trạng Âm thanh
        public bool activeSound; // bật/tắt âm thanh
        public bool activeMusic; // bật/tắt nhạc nền

        // tạo biến lưu trữ phiên bản của config
        public string verConfig = "1.3";
        public string verGame = "Version 1.2";

        public int coin;
        public int highScore;

        public static string[] initInfo = { "1.0", "1", "1", "1", "1", "2", "0", "0", "True", "True" };
        public string[] gameInfo = new string[initInfo.Length];
        // verConfig, bomberStatus, revealerStatus, slowerStatus, dictStatus, levelStatus, coin, highScore, activeSound, activeMusic

        public GameState CurrentGameState = GameState.MainMenu;
        public GameState LastGameState;

        public GameManager()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            Voca_List = new List<VocabularyText>();
            
            soundEffects = new List<SoundEffect>();
        }



        // tạo 3 hàm trả về ảnh theo tình trạng của skill

        public Texture2D getBomberImg()
        {
            switch (bomberStatus)
            {
                case 2:
                    return Content.Load<Texture2D>("Skill/bomber");
                case 3:
                    return Content.Load<Texture2D>("Skill/bomberx1");
                case 4:
                    return Content.Load<Texture2D>("Skill/bomberx2");
                case 5:
                    return Content.Load<Texture2D>("Skill/bomberx3");
                default:
                    return Content.Load<Texture2D>("Skill/bomberfaded");
            }
        }

        public Texture2D getRevealerImg()
        {
            switch (revealerStatus)
            {
                case 2:
                    return Content.Load<Texture2D>("Skill/revealer");
                case 3:
                    return Content.Load<Texture2D>("Skill/revealerx1");
                case 4:
                    return Content.Load<Texture2D>("Skill/revealerx2");
                case 5:
                    return Content.Load<Texture2D>("Skill/revealerx3");
                default:
                    return Content.Load<Texture2D>("Skill/revealerfaded");
            }
        }

        public Texture2D getSlowerImg()
        {
            switch (slowerStatus)
            {
                case 2:
                    return Content.Load<Texture2D>("Skill/slower");
                case 3:
                    return Content.Load<Texture2D>("Skill/slowerx1");
                case 4:
                    return Content.Load<Texture2D>("Skill/slowerx2");
                case 5:
                    return Content.Load<Texture2D>("Skill/slowerx3");
                default:
                    return Content.Load<Texture2D>("Skill/slowerfaded");
            }
        }

        // hàm trả về đường dẫn thư mục chứa Dictionary
        public string getVocaPath()
        {
            if (dictStatus != 4)
                return Path.Combine(Directory.GetCurrentDirectory(), "Vocabulary");
            else
                return Path.Combine(Directory.GetCurrentDirectory(), "Vocabulary\\Custom");
        }

        // tạo 4 hàm trả về ảnh theo trạng thái của Dictionary
        public Texture2D getDict1Img()
        {
            if (dictStatus == 1)
                return Content.Load<Texture2D>("Button/CheckButton/05");
            return Content.Load<Texture2D>("Button/CheckButton/04");
        }

        public Texture2D getDict2Img()
        {
            if (dictStatus == 2)
                return Content.Load<Texture2D>("Button/CheckButton/05");
            return Content.Load<Texture2D>("Button/CheckButton/04");
        }

        public Texture2D getDict3Img()
        {
            if (dictStatus == 3)
                return Content.Load<Texture2D>("Button/CheckButton/05");
            return Content.Load<Texture2D>("Button/CheckButton/04");
        }

        public Texture2D getDict4Img()
        {
            //if (dictStatus == 4)
            //    return Content.Load<Texture2D>("Button/99");
            //return Content.Load<Texture2D>("Button/88");
            return Content.Load<Texture2D>("Button/CheckButton/00");
        }

        public Texture2D getLevel1Img()
        {
            if (levelStatus == 10)
                return Content.Load<Texture2D>("Button/CheckButton/06");
            else if (levelStatus == 3 || levelStatus == 7)
                return Content.Load<Texture2D>("Button/CheckButton/10");
            return Content.Load<Texture2D>("Button/CheckButton/11");
        }

        public Texture2D getLevel2Img()
        {
            if (levelStatus == 10)
                return Content.Load<Texture2D>("Button/CheckButton/06");
            else if (levelStatus == 7 || levelStatus == 9)
                    return Content.Load<Texture2D>("Button/CheckButton/07");
            else if (levelStatus == 2)
                return Content.Load<Texture2D>("Button/CheckButton/10");
            return Content.Load<Texture2D>("Button/CheckButton/11");
        }

        public Texture2D getLevel3Img()
        {
            if (levelStatus == 10 || levelStatus == 2) 
                return Content.Load<Texture2D>("Button/CheckButton/06");
            else if (levelStatus == 3 || levelStatus == 5)
                return Content.Load<Texture2D>("Button/CheckButton/10");
            return Content.Load<Texture2D>("Button/CheckButton/11");
        }

        public int[] getLevel()
        {
            int[] des = new int[2];
            switch (levelStatus)
            {
                case 2:
                    des[0] = 2;
                    des[1] = 5;
                    break;
                case 3:
                    des[0] = 6;
                    des[1] = 9;
                    break;
                case 4:
                    des[0] = 10;
                    des[1] = 13;
                    break;
                case 5:
                    des[0] = 2;
                    des[1] = 9;
                    break;
                case 7:
                    des[0] = 6;
                    des[1] = 13;
                    break;
                case 9:
                    des[0] = 2;
                    des[1] = 13;
                    break;
            }
            return des;
        }

        // tạo hàm trả về số từ sẽ được pull trên mỗi file
        public int getNumberWords()
        {
            return dictStatus * dictStatus * 50 / Directory.GetFiles(getVocaPath()).Length + 1;
        }

        // tạo 2 hàm trả ảnh trạng thái của Sound và Music 
        public Texture2D getSoundImg()
        {
            if (activeSound)
                return Content.Load<Texture2D>("Button/04");
            return Content.Load<Texture2D>("Button/24");
        }

        public Texture2D getMusicImg()
        {
            if (activeMusic)
                return Content.Load<Texture2D>("Button/08");
            return Content.Load<Texture2D>("Button/23");
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        /// 
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            IsMouseVisible = true;

            mainMenu = new MainMenu();

            this.button1.Location = new Drawing.Point(93, 172);
            this.button1.Name = "button1";
            this.button1.Size = new Drawing.Size(75, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "button1";
            this.button1.FlatStyle = Forms.FlatStyle.Standard;
            this.button1.UseVisualStyleBackColor = true;
            //this.button1.Image = Properties.Resources.bt_menu;
            //Forms.Control.FromHandle(Window.Handle).Controls.Add(button1);
        }
        Forms.Button button1 = new Forms.Button();

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            //Resolution: 800x480
            //graphics.IsFullScreen = true;
            //graphics.ApplyChanges();

            // TODO: use this.Content to load your game content here
            Helper.LoadSpriteFont(this);

            levelStatus = 2;

            // load thông tin từ file config
            string filePath = @"config.pww";

            if (!File.Exists(filePath))
            {
                initInfo[0] = verConfig;
                for (int index = 0; index < initInfo.Length; index++)
                {
                    initInfo[index] = MD5.Encrypt(initInfo[index], true);
                }
                File.WriteAllLines(filePath, initInfo);
            }

            gameInfo = File.ReadAllLines(filePath);

            if (MD5.Decrypt(gameInfo[0], true) != verConfig)
            {
                initInfo[0] = verConfig;
                File.Delete(filePath);
                for (int index = 0; index < initInfo.Length; index++)
                {
                    initInfo[index] = MD5.Encrypt(initInfo[index], true);
                }
                File.WriteAllLines(filePath, initInfo);
                gameInfo = File.ReadAllLines(filePath);
            }
            verConfig = MD5.Decrypt(gameInfo[0], true);
            bomberStatus = Convert.ToInt16(MD5.Decrypt(gameInfo[1], true));
            revealerStatus = Convert.ToInt16(MD5.Decrypt(gameInfo[2], true));
            slowerStatus = Convert.ToInt16(MD5.Decrypt(gameInfo[3], true));
            dictStatus = Convert.ToInt16(MD5.Decrypt(gameInfo[4], true));
            levelStatus = Convert.ToInt16(MD5.Decrypt(gameInfo[5], true));
            coin = Convert.ToInt32(MD5.Decrypt(gameInfo[6], true));
            highScore = Convert.ToInt16(MD5.Decrypt(gameInfo[7], true));
            activeSound = Convert.ToBoolean(MD5.Decrypt(gameInfo[8], true));
            activeMusic = Convert.ToBoolean(MD5.Decrypt(gameInfo[9], true));

            // load từ điển
            vocaPath = getVocaPath();
            if (dictStatus != 4)
            {
                //if (!Directory.Exists(vocaPath))
                //{
                //    Directory.CreateDirectory("Vocabulary");
                //}
                foreach (string file in Directory.GetFiles(vocaPath))
                {
                    VocabularyText voca = new VocabularyText();
                    voca.LoadXFromFile(file, getNumberWords());
                    Voca_List.Add(voca);
                }
            }
            else
            {
                VocabularyText voca = new VocabularyText();
                voca.LoadFromFile(Directory.GetFiles(vocaPath)[0]);
                Voca_List.Add(voca);
            }
            

            // load nhạc nền
            MediaPlayer.Play(Content.Load<Song>("Music/carefree"));
            MediaPlayer.IsRepeating = true;
            if (activeMusic)
            {
                MediaPlayer.Volume = 1.0f;
            }
            else
                MediaPlayer.Volume = 0.0f;

            // load hiệu ứng âm thanh
            soundEffects.Add(Content.Load<SoundEffect>("Sound/click_sound_0"));
            soundEffects.Add(Content.Load<SoundEffect>("Sound/click_sound_1"));
            soundEffects.Add(Content.Load<SoundEffect>("Sound/click_sound_2"));
            soundEffects.Add(Content.Load<SoundEffect>("Sound/click_sound_3"));
            soundEffects.Add(Content.Load<SoundEffect>("Sound/click_sound_4"));

            // Play that can be manipulated after the fact
            var instance = soundEffects[0].CreateInstance();
            instance.IsLooped = true;
            if (activeSound)
            {
                SoundEffect.MasterVolume = 1.0f;
            }
            else
            {
                SoundEffect.MasterVolume = 0.0f;
            }
            //instance.Play();

        }

        //private void MediaPlayer_MediaStateChanged(object sender, EventArgs e)
        //{
        //    MediaPlayer.Volume -= 0.1f;
        //    MediaPlayer.Play(song);
        //}

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here

            switch (CurrentGameState)
            {
                case GameState.MainMenu:
                    mainMenu.Update(gameTime);
                    break;
                case GameState.GameStarted:
                case GameState.GamePaused:
                    mainGame.Update(gameTime);
                    break;
                case GameState.GameOver:
                    gameOver.Update(gameTime);
                    break;
                case GameState.Mode:
                    gameMode.Update(gameTime);
                    break;
                case GameState.CustomDict:
                    customDict.Update(gameTime);
                    break;
                case GameState.Setting:
                    settingGame.Update(gameTime);
                    break;
                case GameState.Upgrade:
                    upgradeGame.Update(gameTime);
                    break;
                case GameState.Info:
                    infoGame.Update(gameTime);
                    break;
            }

            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            switch (CurrentGameState)
            {
                case GameState.MainMenu:
                    mainMenu.Draw(spriteBatch);
                    break;
                case GameState.GameStarted:
                case GameState.GamePaused:
                    mainGame.Draw(spriteBatch);
                    break;
                case GameState.GameOver:
                    gameOver.Draw(spriteBatch);
                    break;
                case GameState.Mode:
                    gameMode.Draw(spriteBatch);
                    break;
                case GameState.CustomDict:
                    customDict.Draw(spriteBatch);
                    break;
                case GameState.Setting:
                    settingGame.Draw(spriteBatch);
                    break;
                case GameState.Upgrade:
                    upgradeGame.Draw(spriteBatch);
                    break;
                case GameState.Info:
                    infoGame.Draw(spriteBatch);
                    break;
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
