﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PlayWithWords.Objects
{
    public class AnimationSprite : Sprite
    {
        public int State;
        public Rectangle FrameImage;
        public int Frame;
        public int FrameCount;

        public AnimationSprite() { }

        ~AnimationSprite() { }

        public AnimationSprite(int x, int y, Texture2D image)
        {
            Location = new Vector2(x, y);
            Image = image;
            Frame = 0;
            FrameCount = 1;
            Direction = 0;
            SpriteBounds = new Rectangle(x, y, image.Width / FrameCount, image.Height);
        }

        public void SetState(int state)
        {
            State = state;
            Frame = 0;//reset Frame when change State
        }

        public override void Update(GameTime gameTime)
        {
            SpriteBounds = new Rectangle((int)Location.X, (int)Location.Y, Image.Width / FrameCount, Image.Height);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Image, new Rectangle((int)Location.X, (int)Location.Y, Image.Width, Image.Height), Color.White);
        }

        public override void SetPosition(float x, float y)
        {
            Location.X = x;
            Location.Y = y;
            SpriteBounds = new Rectangle((int)Location.X, (int)Location.Y, Image.Width / FrameCount, Image.Height);
        }
    }
}
