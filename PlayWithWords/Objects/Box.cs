﻿using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using System;
using PlayWithWords.Tools;

namespace PlayWithWords.Objects
{
    public class Box : Sprite
    {
        public string ID { get; set; }
        public int Floor;
        public BoxState BoxState { get; set; }
        Animation DisappearAnimation;
        Character character;
        public Character Character { get { return character; } }
        int cell;
        float dropSpeed;
        public int Cell
        {
            get
            {
                return cell;
            }
            set
            {
                cell = value;
            }
        }

        List<Texture2D> Images;

        public Box() : base()
        {
            Images = new List<Texture2D>();
            for (int i = 0; i <= 11; i++)
            {
                Images.Add(GameManager.Instance.Content.Load<Texture2D>("Box/" + (i < 10 ? "0" : "") + i));
            }
            Image = Images[0];
            Dictionary<int, Frame> animation = new Dictionary<int, Frame>();
            for (int i = 0; i <= 8; i++)
            {
                animation.Add(i, new Frame(Images[i], 150));
            }
            DisappearAnimation = new Animation(animation);
            BoxState = BoxState.None;
            dropSpeed = 1;
        }

        float elapsed = 0;
        public override void Update(GameTime gameTime)
        {
            if (GameManager.Instance.mainGame.isSlow == true)
                dropSpeed = 0.3f;
            else
                dropSpeed = 0.6f;

            elapsed += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            base.Update(gameTime);
            switch (BoxState)
            {
                case BoxState.Dropping:
                    {
                        if (Location.Y >= Parent.Height - ((Floor + 1) * Image.Height))
                        {
                            BoxState = BoxState.Dropped;
                        }
                        else
                        {
                            Location.Y += dropSpeed;
                        }
                    }
                    break;
                case BoxState.Disappearing:
                    {
                        //change frame after delay time in miliseconds
                        if (elapsed >= DisappearAnimation[DisappearAnimation.CurrentFrame].Delay
                            && DisappearAnimation.CurrentFrame < DisappearAnimation.FrameCount - 1)
                        {
                            DisappearAnimation.CurrentFrame++;
                            Image = DisappearAnimation[DisappearAnimation.CurrentFrame].Texture;
                            elapsed = 0;
                            Character.Alpha -= 0.1f;
                        }
                        else if (DisappearAnimation.CurrentFrame == DisappearAnimation.FrameCount - 1)
                        {
                            Character.Alpha = 0.0f;
                            BoxState = BoxState.Disappeared;
                        }
                    }
                    break;
                case BoxState.Disabled:
                    {
                        Image = Images[10];
                    }
                    break;
                default:
                    break;
            }
            if (Image == Images[Focus ? 0 : 11])
            {
                Image = Images[Focus ? 11 : 0];
            }
            if (character != null) character.Location = Location;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            character.Draw(spriteBatch);
        }

        public void Drop()
        {
            BoxState = BoxState.Dropping;
        }

        public void Disappear()
        {
            BoxState = BoxState.Disappearing;
        }

        public void Disable()
        {
            BoxState = BoxState.Disabled;
        }

        public void SetCharacter(char c)
        {
            int num = char.ToUpper(c) - 65;
            character = new Character(c, Helper.Alphabet[num]);
            character.Location = Location;
            character.Parent = Parent;
        }
    }

    public class Character : Sprite
    {
        public char Char { get; }
        public Character(char c, Texture2D texture) : base()
        {
            Char = c;
            Image = texture;
        }
    }

    public enum BoxState
    {
        None,
        Dropping,
        Dropped,
        Disappearing,
        Disappeared,
        Disabled,
    }
}
