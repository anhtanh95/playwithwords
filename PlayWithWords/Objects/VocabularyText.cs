﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PlayWithWords.Objects
{
    public class VocabularyText
    {
        public List<KeyValuePair<string, string>> WordsList;
        public VocabularyText()
        {
            WordsList = new List<KeyValuePair<string, string>>();
        }

        public void LoadFromFile(string filePath)
        {
            string[] lines =  File.ReadAllLines(filePath);
            foreach (string line in lines)
            {
                string[] lineData = RemoveTabChar(line).Split('|');
                //Chỉ load những từ có độ dài từ 2 - 13 chữ cái
                if (lineData[0].Length > 1 && lineData[0].Length < 14)
                {
                    WordsList.Add(new KeyValuePair<string, string>(lineData[0], lineData[1]));
                }
            }
        }

        public void LoadXFromFile(string filePath, int x) // Load X từ trong file
        {
            string[] lines = File.ReadAllLines(filePath);
            int  count = 0;           
            while (count < x)
            {
                Random rnd = new Random();
                int index = rnd.Next(lines.Length);
                string[] lineData = RemoveTabChar(lines[index]).Split('|');
                int[] limit = GameManager.Instance.getLevel();
                if (lineData[0].Length >= limit[0] && lineData[0].Length <= limit[1])
                {
                    WordsList.Add(new KeyValuePair<string, string>(lineData[0], lineData[1]));
                    count++;
                }
                lines = RemoveIndex(lines, index);
            }
        }

        //hàm xóa 1 phần tử tại vị trí index của string[] (xóa phần tử string[index])
        private string[] RemoveIndex(string[] source, int index)
        {
            string[] dest = new string[source.Length - 1];
            if (index > 0)
                Array.Copy(source, 0, dest, 0, index);

            if (index < source.Length - 1)
                Array.Copy(source, index + 1, dest, index, source.Length - index - 1);

            return dest;
        }
        private string RemoveTabChar(string strInput)
        {
            strInput = strInput.Trim().ToLower();
            while (strInput.Contains("\t"))
                strInput = strInput.Replace("\t", "");
            return strInput;
        }
    }
}
