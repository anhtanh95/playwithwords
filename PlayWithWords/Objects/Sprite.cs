﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PlayWithWords.Events;
using PlayWithWords.Input;
using PlayWithWords.Tools;
using System;
using System.Collections.Generic;

namespace PlayWithWords.Objects
{
    public class Sprite
    {
        public Vector2 Location;
        public int Width, Height;
        public float Alpha { get; set; }
        private Texture2D image;
        public Texture2D Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
                if (image != null)
                {
                    Width = image.Width;
                    Height = image.Height;
                    SpriteBounds = new Rectangle((int)Location.X, (int)Location.Y, Width, Height);
                }
            }
        }
        public Rectangle SpriteBounds;
        public int Direction;//0=Left; 1=Right
        public Sprite Parent { get; set; }
        public bool Focus { get; set; }

        #region Mouse Events
        MouseState prevMouseState, currMouseState;

        public event EventHandler LeftButtonClick;
        protected virtual void OnLeftButtonClick(EventArgs e)
        {
            EventHandler handler = LeftButtonClick;
            if (handler != null)
            {
                handler(this, e);
                GameManager.Instance.soundEffects[0].CreateInstance().Play();
            }
        }

        public event EventHandler MouseEnter;
        protected virtual void OnMouseEnter(EventArgs e)
        {
            EventHandler handler = MouseEnter;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler MouseLeave;
        protected virtual void OnMouseLeave(EventArgs e)
        {
            EventHandler handler = MouseLeave;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion

        #region Keyboard Events
        KeyboardState prevKeyboardState, currKeyboardState;

        public event CustomKeyEventHandler KeyDown;
        protected virtual void OnKeyDown(CustomKeyEventArgs e)
        {
            CustomKeyEventHandler handler = KeyDown;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event CustomKeyEventHandler KeyHold;
        protected virtual void OnKeyHold(CustomKeyEventArgs e)
        {
            CustomKeyEventHandler handler = KeyHold;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event CustomKeyEventHandler KeyUp;
        protected virtual void OnKeyUp(CustomKeyEventArgs e)
        {
            CustomKeyEventHandler handler = KeyUp;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion

        public Sprite()
        {
            Direction = 0;
            Location.X = 0;
            Location.Y = 0;
            Width = 0;
            Height = 0;
            Alpha = 1.0f;
            SpriteBounds = new Rectangle((int)Location.X, (int)Location.Y, Width, Height);
        }

        public Sprite(int x, int y, int width, int height)
        {
            Direction = 0;
            Location.X = x;
            Location.Y = y;
            Width = width;
            Height = height;
            Alpha = 1.0f;
            SpriteBounds = new Rectangle((int)Location.X, (int)Location.Y, Width, Height);
        }

        public Sprite(int x, int y, Texture2D image)
        {
            this.image = image;
            Direction = 0;
            Location.X = x;
            Location.Y = y;
            Width = image.Width;
            Height = image.Height;
            Alpha = 1.0f;
            SpriteBounds = new Rectangle((int)Location.X, (int)Location.Y, Width, Height);
        }

        public virtual void Update(GameTime gameTime)
        {
            #region Handle Mouse Events
            if (currMouseState == null) currMouseState = Mouse.GetState();
            prevMouseState = currMouseState;
            currMouseState = Mouse.GetState();

            Rectangle prevMouseRect = new Rectangle(prevMouseState.X, prevMouseState.Y, 1, 1);
            Rectangle currMouseRect = new Rectangle(currMouseState.X, currMouseState.Y, 1, 1);
            bool prevInBound = prevMouseRect.Intersects(SpriteBounds);
            bool currInBound = currMouseRect.Intersects(SpriteBounds);
            if (!prevInBound && currInBound)
            {
                OnMouseEnter(EventArgs.Empty);
            }
            else if (prevInBound && currInBound)
            {
                switch (Helper.GetMousePressState(prevMouseState.LeftButton, currMouseState.LeftButton))
                {
                    case KeyPressState.KeyDown:
                        OnLeftButtonClick(EventArgs.Empty);
                        break;
                }
            }
            else if (prevInBound && !currInBound)
            {
                OnMouseLeave(EventArgs.Empty);
            }
            #endregion

            #region Handle Keyboard Events
            if (currKeyboardState == null) currKeyboardState = Keyboard.GetState();
            prevKeyboardState = currKeyboardState;
            currKeyboardState = Keyboard.GetState();

            if (Focus)
            {
                Dictionary<Keys, KeyStatus> KeyStatusList = new Dictionary<Keys, KeyStatus>();
                //Add all previous keys status to list
                foreach (Keys key in prevKeyboardState.GetPressedKeys())
                {
                    KeyStatus ks = new KeyStatus(key);
                    ks.PrevPressed = true;
                    KeyStatusList.Add(key, ks);
                }
                //Add all current keys status to list
                foreach (Keys key in currKeyboardState.GetPressedKeys())
                {
                    if (!KeyStatusList.ContainsKey(key))
                    {
                        KeyStatus ks = new KeyStatus(key);
                        ks.CurrPressed = true;
                        KeyStatusList.Add(key, ks);
                    }
                    else
                    {
                        KeyStatusList[key].CurrPressed = true;
                    }
                }
                //Handle events for each key in list
                foreach (KeyValuePair<Keys, KeyStatus> keyStatus in KeyStatusList)
                {
                    KeyPressState keyState = Helper.GetKeyPressState(keyStatus.Value.PrevPressed, keyStatus.Value.CurrPressed);
                    switch (keyState)
                    {
                        case KeyPressState.KeyDown:
                            OnKeyDown(new CustomKeyEventArgs(keyStatus.Key));
                            break;
                        case KeyPressState.KeyHold:
                            OnKeyHold(new CustomKeyEventArgs(keyStatus.Key));
                            break;
                        case KeyPressState.KeyUp:
                            OnKeyUp(new CustomKeyEventArgs(keyStatus.Key));
                            break;
                    }
                }
            }
            #endregion
        }

        public class KeyStatus
        {
            public Keys Key { get; set; }
            public bool PrevPressed { get; set; }
            public bool CurrPressed { get; set; }

            public KeyStatus(Keys Key)
            {
                this.Key = Key;
            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            DrawInParent(spriteBatch, image);
        }

        protected void DrawInParent(SpriteBatch spriteBatch, Texture2D img)
        {
            if (image != null)
            {
                if (Parent != null)
                {
                    int hideX = 0;
                    if (Location.X < 0)
                        hideX = 0 - (int)Location.X;
                    else if (Location.X + Width > Parent.Width)
                        hideX = ((int)Location.X + Width) - Parent.Width;

                    int hideY = 0;
                    if (Location.Y < 0)
                        hideY = 0 - (int)Location.Y;
                    else if (Location.Y + Height > Parent.Height)
                        hideY = ((int)Location.Y + Height) - Parent.Height;

                    float x_offset = 0, y_offset = 0;
                    Sprite p = Parent;
                    while (p != null)
                    {
                        x_offset += Parent.Location.X;
                        y_offset += Parent.Location.Y;
                        p = p.Parent;
                    }
                    spriteBatch.Draw(img,
                        new Vector2(x_offset + (Location.X < 0 ? 0 : Location.X), y_offset + (Location.Y < 0 ? 0 : Location.Y)),
                        new Rectangle(Location.X < 0 ? hideX : 0, Location.Y < 0 ? hideY : 0, Width - hideX, Height - hideY),
                        Color.White * Alpha, 0f,
                        Vector2.Zero, 1f,
                        (SpriteEffects)Direction, 0);
                }
                else
                {
                    spriteBatch.Draw(image,
                        new Vector2(Location.X, Location.Y), new Rectangle(0, 0, Width, Height),
                        Color.White * Alpha, 0f,
                        Vector2.Zero, 1f,
                        (SpriteEffects)Direction, 0);
                }
            }
        }

        public virtual void SetPosition(float x, float y)
        {
            Location.X = x;
            Location.Y = y;
            SpriteBounds = new Rectangle((int)Location.X, (int)Location.Y, Width, Height);
        }
    }
}
