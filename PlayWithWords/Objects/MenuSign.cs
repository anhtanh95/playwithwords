﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayWithWords.Controls;
using PlayWithWords.State;
using PlayWithWords.Tools;
using System;
using System.Collections.Generic;

namespace PlayWithWords.Objects
{
    public class MenuSign : Sprite
    {
       
        Dictionary<string, Button> btList = new Dictionary<string, Button>();

        public MenuSign(int x, int y, Texture2D image)
            : base(x, y, image)
        {

            
            Button playButton = new Button(x, y + 3, image.Width, 50, "Play", Helper.MenuFontBlack);
            playButton.LeftButtonClick += new EventHandler(PlayButton_LeftClick);
            playButton.MouseEnter += new EventHandler(PlayButton_MouseEnter);
            playButton.MouseLeave += new EventHandler(PlayButton_MouseLeave);
            btList.Add("play", playButton);

            Button upgradeButton = new Button(x, y + 65, image.Width, 46, "Upgrade", Helper.MenuFontBlack);
            upgradeButton.LeftButtonClick += new EventHandler(UpgradeButton_LeftClick);
            upgradeButton.MouseEnter += new EventHandler(UpgradeButton_MouseEnter);
            upgradeButton.MouseLeave += new EventHandler(UpgradeButton_MouseLeave);
            btList.Add("upgrade", upgradeButton);

            Button modeButton = new Button(x, y + 120, image.Width, 42, "Mode", Helper.MenuFontBlack);
            modeButton.LeftButtonClick += new EventHandler(ModeButton_LeftClick);
            modeButton.MouseEnter += new EventHandler(ModeButton_MouseEnter);
            modeButton.MouseLeave += new EventHandler(ModeButton_MouseLeave);
            btList.Add("mode", modeButton);

            Button settingsButton = new Button(x, y + 177, image.Width, 57, "Settings", Helper.MenuFontBlack);
            settingsButton.LeftButtonClick += new EventHandler(SettingsButton_LeftClick);
            settingsButton.MouseEnter += new EventHandler(SettingsButton_MouseEnter);
            settingsButton.MouseLeave += new EventHandler(SettingsButton_MouseLeave);
            btList.Add("settings", settingsButton);

            Button exitButton = new Button(x, y + 244, image.Width, 44, "Exit", Helper.MenuFontBlack);
            exitButton.LeftButtonClick += ExitButton_LeftButtonClick;
            exitButton.MouseEnter += ExitButton_MouseEnter;
            exitButton.MouseLeave += ExitButton_MouseLeave;
            btList.Add("exit", exitButton);

            Button infoButton = new Button(730, 410, GameManager.Instance.Content.Load<Texture2D>("Button/05"));
            infoButton.LeftButtonClick += InfoButton_LeftButtonClick;
            btList.Add("info", infoButton);

            GameManager.Instance.settingGame = new Setting();
        }

        private void InfoButton_LeftButtonClick(object sender, EventArgs e)
        {
            GameManager.Instance.infoGame = new Information();
            GameManager.Instance.CurrentGameState = GameState.Info;
        }

        private void PlayButton_MouseLeave(object sender, EventArgs e)
        {
            ChangeButtonFont(btList["play"], false);
        }

        private void UpgradeButton_MouseLeave(object sender, EventArgs e)
        {
            ChangeButtonFont(btList["upgrade"], false);
        }

        private void ModeButton_MouseLeave(object sender, EventArgs e)
        {
            ChangeButtonFont(btList["mode"], false);
        }

        private void SettingsButton_MouseLeave(object sender, EventArgs e)
        {
            ChangeButtonFont(btList["settings"], false);
        }

        private void ExitButton_MouseLeave(object sender, EventArgs e)
        {
            ChangeButtonFont(btList["exit"], false);
        }

        private void PlayButton_MouseEnter(object sender, EventArgs e)
        {
            ChangeButtonFont(btList["play"], true);
        }

        private void UpgradeButton_MouseEnter(object sender, EventArgs e)
        {
            ChangeButtonFont(btList["upgrade"], true);
        }

        private void ModeButton_MouseEnter(object sender, EventArgs e)
        {
            ChangeButtonFont(btList["mode"], true);
        }

        private void SettingsButton_MouseEnter(object sender, EventArgs e)
        {
            ChangeButtonFont(btList["settings"], true);
        }

        private void ExitButton_MouseEnter(object sender, EventArgs e)
        {
            ChangeButtonFont(btList["exit"], true);
        }

        private void PlayButton_LeftClick(object sender, EventArgs e)
        {
            GameManager.Instance.mainGame = new MainGame();
            GameManager.Instance.CurrentGameState = GameState.GameStarted;//Chuyển state
            
        }

        private void UpgradeButton_LeftClick(object sender, EventArgs e)
        {
            GameManager.Instance.upgradeGame = new Upgrade();
            GameManager.Instance.CurrentGameState = GameState.Upgrade;

        }

        private void ModeButton_LeftClick(object sender, EventArgs e)
        {
            GameManager.Instance.gameMode = new Mode();
            GameManager.Instance.CurrentGameState = GameState.Mode;
        }

        private void SettingsButton_LeftClick(object sender, EventArgs e)
        {
            GameManager.Instance.LastGameState = GameState.MainMenu;
            GameManager.Instance.CurrentGameState = GameState.Setting;
        }

        private void ExitButton_LeftButtonClick(object sender, EventArgs e)
        {
            GameManager.Instance.Exit();
        }

        private void ChangeButtonFont(Button button, bool enter)
        {
            button.Font = enter ? Helper.MenuFontWhite : Helper.MenuFontBlack;
        }

        public override void Update(GameTime gameTime)
        {
            foreach (KeyValuePair<string, Button> pair in btList)
            {
                Button button = pair.Value;
                if (button != null)
                {
                    button.Update(gameTime);
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            foreach (KeyValuePair<string, Button> pair in btList)
            {
                Button button = pair.Value;
                if (button != null)
                {
                    button.Draw(spriteBatch);
                }
            }
        }
    }
}
