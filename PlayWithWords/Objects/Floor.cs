﻿using System;
using PlayWithWords.Tools;

namespace PlayWithWords.Objects
{
    public class Floor
    {
        public int StartCell { get; }
        public FloorState CurrentState;
        public Word Word { get; }
        public Box[] Cells { get; }

        public Floor(Word Word)
        {
            this.Word = Word;
            CurrentState = FloorState.None;
            Cells = new Box[Word.Count];
            StartCell = (Helper.MaxCellPerFloor - Word.Count) / 2;
        }

        public void Destroy()
        {
            CurrentState = FloorState.Destroying;
            for (int i = 0; i < Cells.Length; i++)
            {
                Cells[i].Disappear();
            }
        }

        public void Disable()
        {
            CurrentState = FloorState.Disabled;
            for (int i = 0; i < Cells.Length; i++)
            {
                Cells[i].Disable();
            }
        }

        public bool CheckWordOrder()
        {
            for (int i = 0; i < Word.Count; i++)
            {
                if (Word.OriginOrder[i] != Cells[i].Character.Char) return false;
            }
            return true;
        }

        public void Update()
        {
            if (CurrentState == FloorState.Destroying)
            {
                bool disappeared = true;
                foreach (Box box in Cells)
                {
                    if (box != null && box.BoxState != BoxState.Disappeared)
                    {
                        disappeared = false;
                        break;
                    }
                }
                if (disappeared)
                {
                    CurrentState = FloorState.Destroyed;
                }
            }
        }

        internal void Drop()
        {
            foreach (Box box in Cells)
            {
                if (box != null) box.Drop();
            }
        }
    }

    public enum FloorState
    {
        None,
        Destroying,
        Destroyed,
        Disabled,
    }
}
