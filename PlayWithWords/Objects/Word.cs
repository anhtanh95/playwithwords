﻿using System;
using System.Collections.Generic;

namespace PlayWithWords.Objects
{
    public class Word
    {
        public string EMeaning { get; }
        public string VMeaning { get; }
        public List<char> OriginOrder { get; }
        public List<char> RandomOrder { get; }
        int currentCharOrder;

        public Word(string eMeaning, string vMeaning)
        {
            EMeaning = eMeaning;
            VMeaning = vMeaning;
            OriginOrder = new List<char>();
            RandomOrder = new List<char>();
            foreach (char c in eMeaning)
            {
                OriginOrder.Add(c);
            }
            Random rnd = new Random();
            //copy current list of char to a temporary list
            List<char> temp = new List<char>(OriginOrder);
            for (int i = 0; i < OriginOrder.Count; i++)
            {
                int x = rnd.Next(temp.Count);
                RandomOrder.Add(temp[x]);
                temp.Remove(temp[x]);
            }
            currentCharOrder = 0;
        }

        public int Count { get { return OriginOrder.Count; } }
        public int GetCurrentChar()
        {
            return currentCharOrder;
        }

        public int NextChar()
        {
            if (currentCharOrder < OriginOrder.Count - 1)
            {
                currentCharOrder++;
            }
            else
            {
                currentCharOrder = -1;
            }
            return currentCharOrder;
        }
    }
}
