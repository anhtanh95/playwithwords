﻿using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace PlayWithWords.Objects
{
    public class Animation
    {
        Dictionary<int, Frame> Frames;
        public int CurrentFrame { get; set; }
        public int FrameCount { get { return Frames.Count; } }

        public Frame this[int index] { get { return Frames[index]; } }

        public Animation(Dictionary<int, Frame> frames)
        {
            Frames = frames;
            CurrentFrame = 0;
        }
    }

    public class Frame
    {
        public Texture2D Texture { get; }
        public float Delay { get; }

        public Frame(Texture2D Texture, float Delay)
        {
            this.Texture = Texture;
            this.Delay = Delay;
        }
    }
}
