﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PlayWithWords.Objects;
using PlayWithWords.Tools;

namespace PlayWithWords.Controls
{
    public class Button : Sprite
    {
        public string Text = "";
        public SpriteFont Font = null;

        public Button(int x, int y, Texture2D image)
            : base(x, y, image)
        {
        }

        public Button(int x, int y, Texture2D image, string text, SpriteFont font)
            : base(x, y, image)
        {
            Text = text;
            Font = font;
        }

        public Button(int x, int y, int width, int height, string text, SpriteFont font)
            : base(x, y, width, height)
        {
            Text = text;
            Font = font;
        }

        /*private bool IsRegionTransparent(MouseState mouse)
        {
            return GetPixel(mouse.X, mouse.Y, ImageWidth) == Color.Transparent;
        }

        public Color GetPixel(int x, int y, int width)
        {
            Color[] colors = GetPixels();
            return colors[x + (y * width)];
        }

        public Color[] GetPixels()
        {
            Color[] colors1D = new Color[ImageWidth * ImageHeight];
            Image.GetData(colors1D);
            return colors1D;
        }*/

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            if (!Text.Equals(""))
            {
                Helper.DrawString(spriteBatch, Font, Text, SpriteBounds, Helper.Alignment.Center, Color.White);
            }
        }

        public void SetText(string text, SpriteFont font)
        {
            Text = text;
            Font = font;
        }
    }
}
