﻿using Microsoft.Xna.Framework.Graphics;
using PlayWithWords.Objects;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace PlayWithWords.Controls
{
    public class Panel : Sprite
    {
        public Dictionary<string, Sprite> Sprites;

        public Panel(int x, int y, Texture2D image)
            : base(x, y, image)
        {
            Sprites = new Dictionary<string, Sprite>();
        }

        public Panel(int x, int y, int width, int height, Texture2D image)
            : base(x, y, width, height)
        {
            Image = image;
            Sprites = new Dictionary<string, Sprite>();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            foreach (KeyValuePair<string, Sprite> sprite in Sprites)
            {
                sprite.Value.Update(gameTime);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);

            foreach (KeyValuePair<string, Sprite> sprite in Sprites)
            {
                sprite.Value.Draw(spriteBatch);
            }
        }

        public void AddSprite(string ID, Sprite sprite)
        {
            sprite.Parent = this;
            Sprites.Add(ID, sprite);
        }
    }
}
