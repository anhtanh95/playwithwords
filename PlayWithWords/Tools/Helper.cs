﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PlayWithWords.Input;
using System.Collections.Generic;
using System.Text;

namespace PlayWithWords.Tools
{
    public class Helper
    {
        //Số tầng tối đa: mặc định khởi tạo tối đa được xếp 10 tầng
        public static int MaxFloor = 10;
        //Số ô tối đa mỗi tầng: số ô tối đa cho phép xếp hộp trên 1 tầng, mặc định khởi tạo là 13 ô
        public static int MaxCellPerFloor = 13;
        public static SpriteFont Arial14,
            MenuFontBlack, MenuFontWhite,
            ComicSansMS20, ComicSansMS25,
            Altus20, Altus25,
            Thinpaw,
            Pacifico,
            Orial, Critter,
            Albino, Fato;
        public static List<Texture2D> Alphabet = new List<Texture2D>();

        public enum Alignment { Center = 0, Left = 1, Right = 2, Top = 4, Bottom = 8 }

        public static void DrawString(SpriteBatch spriteBatch, SpriteFont font, string text, Rectangle bounds, Alignment align, Color color)
        {
            Vector2 size = font.MeasureString(text);
            Vector2 pos = new Vector2(bounds.Center.X, bounds.Center.Y);
            Vector2 origin = size * 0.5f;
            if (align.HasFlag(Alignment.Left))
                origin.X += bounds.Width / 2 - size.X / 2;

            if (align.HasFlag(Alignment.Right))
                origin.X -= bounds.Width / 2 - size.X / 2;

            if (align.HasFlag(Alignment.Top))
                origin.Y += bounds.Height / 2 - size.Y / 2;

            if (align.HasFlag(Alignment.Bottom))
                origin.Y -= bounds.Height / 2 - size.Y / 2;

            spriteBatch.DrawString(font, text, pos, color, 0, origin, 1, SpriteEffects.None, 0);
        }

        public static void LoadSpriteFont(GameManager GM)
        {
            Arial14 = GM.Content.Load<SpriteFont>("Fonts/Arial14");
            ComicSansMS20 = GM.Content.Load<SpriteFont>("Fonts/ComicSansMS20");
            ComicSansMS25 = GM.Content.Load<SpriteFont>("Fonts/ComicSansMS25");
            MenuFontBlack = GM.Content.Load<SpriteFont>("Fonts/ActionJackson");
            Altus20 = GM.Content.Load<SpriteFont>("Fonts/Altus20");
            Altus25 = GM.Content.Load<SpriteFont>("Fonts/Altus25");
            MenuFontWhite = GM.Content.Load<SpriteFont>("Fonts/ActionJacksonWhite");
            Fato = GM.Content.Load<SpriteFont>("Fonts/Fato");
            Critter = GM.Content.Load<SpriteFont>("Fonts/Critter");
            Albino = GM.Content.Load<SpriteFont>("Fonts/Albino");
            Pacifico = GM.Content.Load<SpriteFont>("Fonts/Pacifico");
            for (int i = 0; i < 26; i++)
            {
                Alphabet.Add(GM.Content.Load<Texture2D>("Alphabet/" + (i < 10 ? "0" : "") + i));
            }
        }

        public static string WrapText(SpriteFont spriteFont, string text, float maxLineWidth)
        {
            string[] words = text.Split(' ');
            StringBuilder sb = new StringBuilder();
            float lineWidth = 0f;
            float spaceWidth = spriteFont.MeasureString(" ").X;

            foreach (string word in words)
            {
                Vector2 size = spriteFont.MeasureString(word);

                if (lineWidth + size.X < maxLineWidth)
                {
                    sb.Append(word + " ");
                    lineWidth += size.X + spaceWidth;
                }
                else
                {
                    sb.Append("\n" + word + " ");
                    lineWidth = size.X + spaceWidth;
                }
            }

            return sb.ToString();
        }

        public static KeyPressState GetMousePressState(ButtonState prev, ButtonState curr)
        {
            if (prev == ButtonState.Released && curr == ButtonState.Pressed)
            {
                return KeyPressState.KeyDown;
            }
            else
            if (prev == ButtonState.Pressed && curr == ButtonState.Pressed)
            {
                return KeyPressState.KeyHold;
            }
            else
            if (prev == ButtonState.Pressed && curr == ButtonState.Released)
            {
                return KeyPressState.KeyUp;
            }
            else
            {
                return KeyPressState.None;
            }
        }

        public static KeyPressState GetKeyPressState(bool prev, bool curr)
        {
            if (!prev && curr)
            {
                return KeyPressState.KeyDown;
            }
            else
            if (prev && curr)
            {
                return KeyPressState.KeyHold;
            }
            else
            if (prev && !curr)
            {
                return KeyPressState.KeyUp;
            }
            else
            {
                return KeyPressState.None;
            }
        }
    }
}
