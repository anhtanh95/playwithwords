﻿namespace PlayWithWords.Input
{
    public enum KeyPressState
    {           //Previous  Current     Description
        None,   //Released  Released    The button is not pressed at all.
        KeyDown,//Released  Pressed     The button has been pressed for the first time between the last Update and this one.
        KeyHold,//Pressed   Pressed     The button is being held down.
        KeyUp,  //Pressed   Released    The button has been released since the previous Update.
    }
}
