﻿using System;

namespace PlayWithWords
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            GameManager.Instance = new GameManager();
            using (var game = GameManager.Instance)
                game.Run();
        }
    }
#endif
}
