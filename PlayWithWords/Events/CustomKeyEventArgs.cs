﻿using Microsoft.Xna.Framework.Input;
using System;

namespace PlayWithWords.Events
{
    public delegate void CustomKeyEventHandler(object sender, CustomKeyEventArgs e);
    public class CustomKeyEventArgs : EventArgs
    {
        public Keys Key { get; set; }

        public CustomKeyEventArgs(Keys key)
        {
            Key = key;
        }
    }
}
